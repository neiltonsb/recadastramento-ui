import api from '~/services/api';

export async function find({ page, rows, filtro }) {
  return api
    .get('cargos', {
      params: {
        ...filtro,
        page,
        size: rows,
      },
    })
    .then(response => {
      const data = {
        paginator: {
          rows: response.data.size,
          totalRecords: response.data.totalElements,
        },
        content: response.data.content,
      };
      return data;
    });
}

export async function create(work) {
  return api.post('cargos', work);
}

export async function update(work) {
  return api.put(`cargos/${work.codigo}`, work);
}

export async function remove(codigo) {
  return api.delete(`cargos/${codigo}`);
}
