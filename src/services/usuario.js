import api from '~/services/api';

export async function find({ page, rows, filtro }) {
  return api
    .get('usuarios', {
      params: {
        ...filtro,
        page,
        size: rows,
      },
    })
    .then(response => {
      const data = {
        paginator: {
          rows: response.data.size,
          totalRecords: response.data.totalElements,
        },
        content: response.data.content,
      };
      return data;
    });
}

export async function create(usuario) {
  return api.post('usuarios', usuario);
}

export async function update(usuario) {
  return api.put(`usuarios/${usuario.codigo}`, usuario);
}

export async function remove(codigo) {
  return api.delete(`usuarios/${codigo}`);
}
