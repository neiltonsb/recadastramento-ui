import api from './api';

export async function update(perfil) {
  return api.put('perfil', perfil);
}
