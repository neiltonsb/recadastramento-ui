import qs from 'qs';
import api from '~/services/api';

export function find({ page, rows, filtro }) {
  return api
    .get('servidores', {
      params: {
        ...filtro,
        page,
        size: rows,
      },
      paramsSerializer: params => {
        return qs.stringify(params, { arrayFormat: 'repeat' });
      },
    })
    .then(response => {
      const data = {
        paginator: {
          rows: response.data.size,
          totalRecords: response.data.totalElements,
        },
        content: response.data.content,
      };
      return data;
    });
}

export function findByCpf(cpf) {
  return api.get(`servidores/${cpf}`);
}

export function enable(servidor) {
  return api.post('servidores', servidor);
}

export function remove(codigo) {
  return api.delete(`servidores/${codigo}`);
}
