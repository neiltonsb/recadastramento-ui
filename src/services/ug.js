import api from '~/services/api';

export async function find({ page, rows, filtro }) {
  return api
    .get('unidades_gestoras', {
      params: {
        ...filtro,
        page,
        size: rows,
      },
    })
    .then(response => {
      const data = {
        paginator: {
          rows: response.data.size,
          totalRecords: response.data.totalElements,
        },
        content: response.data.content,
      };
      return data;
    });
}

export async function create(ug) {
  return api.post('unidades_gestoras', ug);
}

export async function update(ug) {
  return api.put(`unidades_gestoras/${ug.codigo}`, ug);
}

export async function remove(codigo) {
  return api.delete(`unidades_gestoras/${codigo}`);
}
