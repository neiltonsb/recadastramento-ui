import { parseISO, isBefore, format } from 'date-fns';
import api from '~/services/api';

export async function hasExpired() {
  return api.get('campanhas').then(({ data: campaign }) => {
    const end = parseISO(campaign.dataFim);
    const now = new Date();

    return isBefore(end, now);
  });
}

export async function getExpirationDate() {
  return api.get('campanhas').then(({ data: campaign }) => {
    const end = parseISO(campaign.dataFim);

    return format(end, 'dd/MM/yyyy');
  });
}

export async function get() {
  return api.get('campanhas');
}

export async function update(campaign) {
  return api.put(`campanhas/${campaign.codigo}`, campaign);
}
