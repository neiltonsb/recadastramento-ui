import { toast } from 'react-toastify';

const base = process.env.REACT_APP_ROUTER_BASE || '';

export function handle({ response }) {
  let msg =
    'Não conseguimos processar sua solicitação, tente novamente dentro de instantes';

  if (response.status >= 500) {
    msg = 'Erro interno do servidor';
  }

  if (response.status === 404) {
    msg = 'Recurso não encontrado';
  }

  if (response.status === 403) {
    msg = 'Você não está autorizado a acessar este recurso';
    window.location = `${base}/403`;
  }

  if (response.status === 400) {
    const { data } = response;
    try {
      if (data.error === 'invalid_grant') {
        msg = 'Usuário inexistente ou senha inválida';
      } else if (data.error === 'usuario_inativo') {
        msg = 'Usuário inativo';
      } else if (data.error === 'campanha_expirada') {
        window.location = `${base}/campanha_encerrada`;
      }

      msg = data[0].message;
    } catch (e) {} // eslint-disable-line
  }

  toast.error(msg);

  return Promise.reject(response);
}
