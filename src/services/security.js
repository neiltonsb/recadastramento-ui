import { store } from '~/store';

export function hasRole(role) {
  const { authorities } = store.getState().auth;
  return authorities && authorities.includes(role);
}

export function hasAnyRole(roles) {
  return roles.map(role => {
    return hasRole(role);
  });
}
