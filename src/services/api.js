import axios from 'axios';

import { store } from '~/store';
import { signOut } from '~/store/modules/auth/actions';

const api = axios.create({
  // baseURL: 'http://srvcontabil.sefin.ro.gov.br/recad',
  baseURL: 'http://localhost:8080',
});

api.interceptors.request.use(config => {
  const { url } = config;
  const base = process.env.REACT_APP_ROUTER_BASE || '';

  if (
    url !== '/oauth/token' &&
    window.location.pathname !== `${base}/` &&
    window.location.pathname !== `${base}/campanha_encerrada`
  ) {
    const { access_token } = store.getState().auth;
    config.headers.Authorization = `Bearer ${access_token}`;
  }

  return config;
});

api.interceptors.response.use(
  response => response,
  error => {
    const { status } = error.response;

    if (status === 401) {
      store.dispatch(signOut());
    }

    return Promise.reject(error);
  }
);

export default api;
