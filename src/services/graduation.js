import api from '~/services/api';

export async function find({ page, rows, filtro }) {
  return api
    .get('formacoes', {
      params: {
        ...filtro,
        page,
        size: rows,
      },
    })
    .then(response => {
      const data = {
        paginator: {
          rows: response.data.size,
          totalRecords: response.data.totalElements,
        },
        content: response.data.content,
      };
      return data;
    });
}

export async function create(graduation) {
  return api.post('formacoes', graduation);
}

export async function update(graduation) {
  return api.put(`formacoes/${graduation.codigo}`, graduation);
}

export async function remove(codigo) {
  return api.delete(`formacoes/${codigo}`);
}
