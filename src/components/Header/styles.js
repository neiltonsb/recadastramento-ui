import styled from 'styled-components';
import media from 'styled-media-query';
import { Link } from 'react-router-dom';

export const Container = styled.div`
  background: #006699;
  padding: 0 30px;
  box-shadow: 0px 0px 3px 1px #000;

  * {
    font-family: 'Roboto', monospace, sans-serif;
  }
`;

export const Content = styled.div`
  height: 65px;
  max-width: 1400px;
  margin: 0 auto;

  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Icon = styled.div`
  svg {
    vertical-align: middle;

    &:hover {
      color: #ccc !important;
      cursor: pointer;
    }
  }
`;

export const Title = styled.div`
  display: none;

  ${media.greaterThan('750px')`  
    display: flex;
    flex-direction: column;
    align-items: center;

    color: #fff;

    span:first-child {
      width: 100%;
      text-align: center;
      font-weight: bold;
      padding: 0 0 5px;
      margin: 0 0 5px;
      border-bottom: 1px solid rgba(255, 255, 255, 0.3);
    }
  `}
`;

export const Left = styled.div`
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    color: #eee;
  }
`;

export const ProfileButton = styled(Link)`
  margin: 0 20px;
  cursor: pointer;
  color: #fff;

  &:hover {
    color: #ccc;
  }
`;

export const SignOutButton = styled.span`
  cursor: pointer;

  &:hover {
    color: #ccc;
  }
`;
