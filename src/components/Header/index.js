import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { FaSignOutAlt, FaUserAlt, FaBars } from 'react-icons/fa';

import {
  Container,
  Content,
  Icon,
  Title,
  Left,
  ProfileButton,
  SignOutButton,
} from './styles';

import { signOut } from '~/store/modules/auth/actions';

export default function Header({ sideBar, setSideBar }) {
  const { user } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  return (
    <Container>
      <Content>
        <Icon onClick={() => setSideBar(!sideBar)}>
          <FaBars size={28} color="#eee" />
        </Icon>

        <Title>
          <span>A A A</span>
          <span>Sistema de Recadastramento</span>
        </Title>

        <Left>
          <div>
            <span>{user}</span>
            <ProfileButton to="/admin/perfil" title="Perfil">
              <FaUserAlt />
            </ProfileButton>
            <SignOutButton
              title="Sair do sistema"
              onClick={() => dispatch(signOut())}
            >
              <FaSignOutAlt />
            </SignOutButton>
          </div>
        </Left>
      </Content>
    </Container>
  );
}
