import React from 'react';

import { FaCheck } from 'react-icons/fa';

import { Container, Content, Icon, Message, Actions, Button } from './styles';

export default function FeedbackAlert({ title, message, onClose }) {
  return (
    <Container>
      <Content>
        <Icon>
          <FaCheck size="64" color="#34A835" />
        </Icon>
        <Message>{message}</Message>

        <Actions>
          <Button type="button" onClick={onClose}>
            Fechar
          </Button>
        </Actions>
      </Content>
    </Container>
  );
}
