import styled from 'styled-components';

import { darken } from 'polished';

export const Container = styled.div`
  width: 600px;

  * {
    font-family: 'Roboto', monospace, sans-serif;
  }
`;

export const Content = styled.div`
  padding: 30px;
  background: #fff;
  border-radius: 10px;
  box-shadow: 0 20px 75px rgba(0, 0, 0, 0.13);
`;

export const Icon = styled.div`
  text-align: center;
`;

export const Message = styled.p`
  margin: 10px 0;
  color: #333;
  text-align: center;
`;

export const Actions = styled.div`
  text-align: center;
`;

export const Button = styled.button`
  margin-right: 10px;
  border: none;
  background: #006699;
  color: #fff;
  border-radius: 4px;
  font-weight: bold;

  width: 50px;
  height: 25px;

  &:hover {
    background: ${darken(0.1, '#006699')};
  }
`;
