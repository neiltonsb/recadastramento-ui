import React, { useState, useEffect } from 'react';

import { MultiSelect } from 'primereact/multiselect';
import api from '~/services/api';

export default function GrupoMultiSelect({ ...rest }) {
  const [grupos, setGrupos] = useState([]);
  useEffect(() => {
    async function loadGrupos() {
      const { data } = await api.get('grupos?list');
      setGrupos(
        data.map(grupo => ({
          value: { codigo: grupo.codigo },
          label: grupo.nome,
        }))
      );
    }

    loadGrupos();
  }, []);

  return <MultiSelect {...rest} options={grupos} />;
}
