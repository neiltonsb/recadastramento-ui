import React, { useState, useEffect } from 'react';
import { Dropdown } from 'primereact/dropdown';
import api from '~/services/api';

export default function UnidadeGestoraDropdown({ ...rest }) {
  const [ugs, setUgs] = useState([]);
  useEffect(() => {
    async function loadUgs() {
      const { data } = await api.get('unidades_gestoras?list');
      setUgs(
        data.map(ug => ({
          value: {
            codigo: ug.codigo,
            codigoUg: ug.codigoUg,
            nome: ug.nome,
          },
          label: `${ug.codigoUg} - ${ug.nome}`,
        }))
      );
    }

    loadUgs();
  }, []);

  return <Dropdown {...rest} options={ugs} />;
}
