import React, { useState, useEffect } from 'react';
import { MultiSelect } from 'primereact/multiselect';
import api from '~/services/api';

export default function UnidadeGestoraMultiSelect({ ...rest }) {
  const [ugs, setUgs] = useState([]);
  useEffect(() => {
    async function loadUgs() {
      const { data } = await api.get('unidades_gestoras?list');
      setUgs(
        data.map(ug => ({
          value: ug.codigo,
          label: `${ug.codigoUg} - ${ug.nome}`,
        }))
      );
    }

    loadUgs();
  }, []);

  return <MultiSelect {...rest} options={ugs} />;
}
