import React, { useState, useEffect } from 'react';
import { Dropdown } from 'primereact/dropdown';
import api from '~/services/api';

export default function FormacaoDropdown({ ...rest }) {
  const [formacoes, setFormacoes] = useState([]);
  useEffect(() => {
    async function loadFormacoes() {
      const { data } = await api.get('formacoes?list');
      setFormacoes(
        data.map(formacao => ({
          value: { codigo: formacao.codigo, nome: formacao.nome },
          label: formacao.nome,
        }))
      );
    }

    loadFormacoes();
  }, []);

  return <Dropdown {...rest} options={formacoes} />;
}
