import styled from 'styled-components';

import { darken } from 'polished';

export const Container = styled.div`
  width: 400px;

  * {
    font-family: 'Roboto', monospace, sans-serif;
  }
`;

export const Content = styled.div`
  padding: 30px;
  background: #fff;
  border-radius: 10px;
  box-shadow: 0 20px 75px rgba(0, 0, 0, 0.13);
`;

export const Title = styled.h2`
  color: #333;
`;

export const Message = styled.p`
  color: #333;
  margin: 10px 0;
`;

export const Actions = styled.div``;

export const ConfirmButton = styled.button`
  margin-right: 10px;
  border: none;
  background: #006699;
  color: #fff;
  border-radius: 4px;
  font-weight: bold;

  width: 50px;
  height: 25px;

  &:hover {
    background: ${darken(0.1, '#006699')};
  }
`;

export const CancelButton = styled.button`
  margin-right: 10px;
  border: none;
  background: #c01120;
  color: #fff;
  border-radius: 4px;
  font-weight: bold;

  width: 50px;
  height: 25px;

  &:hover {
    background: ${darken(0.1, '#c01120')};
  }
`;
