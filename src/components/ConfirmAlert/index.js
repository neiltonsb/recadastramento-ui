import React from 'react';

import {
  Container,
  Content,
  Title,
  Message,
  Actions,
  ConfirmButton,
  CancelButton,
} from './styles';

export default function ConfirmAlert({ title, message, onConfirm, onClose }) {
  return (
    <Container>
      <Content>
        <Title>{title}</Title>
        <Message>{message}</Message>

        <Actions>
          <ConfirmButton type="button" onClick={onConfirm}>
            Sim
          </ConfirmButton>
          <CancelButton type="button" onClick={onClose}>
            Não
          </CancelButton>
        </Actions>
      </Content>
    </Container>
  );
}
