import React, { useState, useEffect } from 'react';
import { Dropdown } from 'primereact/dropdown';
import api from '~/services/api';

export default function CargoDropdown({ ...rest }) {
  const [cargos, setCargos] = useState([]);
  useEffect(() => {
    async function loadCargos() {
      const { data } = await api.get('cargos?list');
      setCargos(
        data.map(cargo => ({
          value: { codigo: cargo.codigo, nome: cargo.nome },
          label: cargo.nome,
        }))
      );
    }

    loadCargos();
  }, []);

  return <Dropdown {...rest} options={cargos} />;
}
