import { createGlobalStyle } from 'styled-components';

import 'react-toastify/dist/ReactToastify.css';

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
  
  /* Global Styles */
  * {
    margin: 0;
    padding: 0;
    outline: none;
    box-sizing: border-box;
  }

  *:focus {
    outline: 0;
  }

  html, body, #root {
    height: 100%;
  }

  body {
    -webkit-font-smoothing: antialiased;
  }


  a {
    text-decoration: none;
  }

  ul {
    list-style: none;
  }

  button {
    cursor: pointer;
  }
  /****************************/

  /* Custom Prime React */
  .p-dropdown-filter-container {
    width: 100% !important;
  }
  
  .p-multiselect-filter-container {
    width: 90% !important;
  }
  .p-sidebar {
    padding-left: 0 !important;
    padding-right: 0 !important;
    border: 0 !important;
  }

  .p-sidebar-close {
    margin-right: 10px !important;
  }

  .p-messages {
    margin-top: 0 !important;
  }

  @media screen and (max-width: 40em) {
    .p-datatable.p-datatable-responsive .p-datatable-tbody > tr > td {
      text-align: left !important;
    }
  }
  /****************************/

  /* My Styles */
  .badge {
    background: #6c757d;
    color: #fff;
    font-weight: bold;
    padding: 5px;
    border-radius: 4px;
    font-size: 12px;
  }

  .badge.badge-success {
    background: #0C6B0D;
  }

  .badge.badge-danger {
    background: #e91224;
  }
  /****************************/
`;
