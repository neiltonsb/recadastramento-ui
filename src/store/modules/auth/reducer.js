import produce from 'immer';

const INITIAL_STATE = {
  access_token: null,
  user: null,
  user_id: null,
  authorities: [],
  signed: false,
  loading: false,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@auth/SIGN_IN_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@auth/SIGN_IN_SUCCESS': {
        draft.access_token = action.payload.access_token;
        draft.user = action.payload.user;
        draft.user_id = action.payload.user_id;
        draft.authorities = action.payload.authorities;

        draft.signed = true;
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_OUT': {
        draft.access_token = null;
        draft.user = null;
        draft.user_id = null;
        draft.authorities = null;
        draft.signed = false;
        break;
      }
      default:
    }
  });
}
