import { call, all, put, takeLatest } from 'redux-saga/effects';
import jwt from 'jsonwebtoken';

import { signInSuccess, signFailure } from '~/store/modules/auth/actions';
import api from '~/services/api';
import * as ExceptionService from '~/services/exception';

const base = process.env.REACT_APP_ROUTER_BASE || '';

export function* signIn({ payload }) {
  try {
    const { login, password } = payload;

    api.defaults.headers.Authorization = 'Basic YW5ndWxhcjpAbmd1bEByMA==';
    api.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';

    const body = `username=${login}&password=${password}&grant_type=password`;
    const response = yield call(api.post, '/oauth/token', body, {
      withCredentials: true,
    });

    const { access_token } = response.data;
    const { authorities, user, user_id } = jwt.decode(access_token);

    yield put(signInSuccess(access_token, user, user_id, authorities));
    window.location = `${base}/admin/servidores`;
  } catch (err) {
    yield put(signFailure());
    ExceptionService.handle(err);
  }
}

export function signOut() {
  window.location = `${base}/login`;
}

export default all([
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/SIGN_OUT', signOut),
]);
