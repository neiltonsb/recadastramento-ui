import React, { useEffect } from 'react';
import { parseISO, isAfter } from 'date-fns';

import { Button } from 'primereact/button';
import { Message } from 'primereact/message';
import { Calendar } from 'primereact/calendar';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';
import { toast } from 'react-toastify';

import * as CampaignService from '~/services/campaign';
import * as ExceptionService from '~/services/exception';

import { Content, Navbar } from './styles';

const form = FormBuilder.group({
  codigo: [''],
  dataInicio: ['', Validators.required],
  dataFim: ['', Validators.required],
});

const pt = {
  firstDayOfWeek: 1,
  dayNames: [
    'domingo',
    'segunda',
    'terça',
    'quarta',
    'quinta',
    'sexta',
    'sábado',
  ],
  dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
  dayNamesMin: ['Do', 'Se', 'Te', 'Qa', 'Qi', 'Sx', 'Sa'],
  monthNames: [
    'janeiro',
    'fevereiro',
    'março',
    'abril',
    'maio',
    'junho',
    'julho',
    'agosto',
    'setembro',
    'outubro',
    'novembro',
    'dezembro',
  ],
  monthNamesShort: [
    'jan',
    'fev',
    'mar',
    'abr',
    'mai',
    'jun',
    'jul',
    'ago',
    'set',
    'out',
    'nov',
    'dez',
  ],
  today: 'Hoje',
  clear: 'Limpar',
  dateFormat: 'dd/mm/yy',
  weekHeader: 'Sm',
};

export default function Campaign() {
  useEffect(() => {
    async function loadCampaign() {
      const { data: campaign } = await CampaignService.get().catch(error =>
        ExceptionService.handle(error)
      );

      form.patchValue({
        ...campaign,
        dataInicio: parseISO(campaign.dataInicio),
        dataFim: parseISO(campaign.dataFim),
      });
    }

    loadCampaign();
  });

  async function handleSubmit() {
    const dataInicio = form.get('dataInicio').value;
    const dataFim = form.get('dataFim').value;

    if (isAfter(dataInicio, dataFim)) {
      form.get('dataFim').setErrors({ lowerDate: true });
    } else {
      await CampaignService.update(form.value)
        .then(() => {
          toast.success('Campanha salva com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    }
  }

  return (
    <Content>
      <Navbar>
        <h1>Campanha</h1>
      </Navbar>

      <FieldGroup
        control={form}
        render={({ invalid }) => (
          <form>
            <div className="p-grid p-fluid">
              <div className="p-col-12 p-md-6 p-sm-12">
                <FieldControl name="dataInicio">
                  {({ handler, touched, hasError }) => (
                    <>
                      <Calendar
                        {...handler()}
                        placeholder="Data inicio"
                        locale={pt}
                        dateFormat="dd/mm/yy"
                      />
                      {touched && hasError('required') && (
                        <Message
                          severity="error"
                          text="Data inicio obrigatória"
                        />
                      )}
                    </>
                  )}
                </FieldControl>
              </div>

              <div className="p-col-12 p-md-6 p-sm-12">
                <FieldControl name="dataFim">
                  {({ handler, touched, hasError }) => (
                    <>
                      <Calendar
                        {...handler()}
                        placeholder="Data fim"
                        dateFormat="dd/mm/yy"
                        locale={pt}
                      />
                      {touched && hasError('required') && (
                        <Message severity="error" text="Data fim obrigatória" />
                      )}

                      {touched && hasError('lowerDate') && (
                        <Message
                          severity="error"
                          text="Data fim deve ser posterior a data de inicio"
                        />
                      )}
                    </>
                  )}
                </FieldControl>
              </div>
            </div>

            <Button
              type="button"
              disabled={invalid}
              label="Salvar"
              icon="pi pi-check"
              onClick={handleSubmit}
            />
          </form>
        )}
      />
    </Content>
  );
}
