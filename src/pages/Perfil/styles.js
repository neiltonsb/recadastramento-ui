import styled from 'styled-components';

export const Content = styled.div`
  margin: 20px 0;

  display: flex;
  align-items: center;
  justify-content: center;

  form {
    box-shadow: 1px 0px 8px 5px #eeee;
    padding: 20px;
  }
`;

export const User = styled.p`
  font-family: 'Roboto', monospace, sans-serif;
  color: #666;
  font-size: 18px;
  margin-bottom: 20px;
  text-align: center;
`;
