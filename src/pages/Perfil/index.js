import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Password } from 'primereact/password';
import { Message } from 'primereact/message';
import { Button } from 'primereact/button';

import { toast } from 'react-toastify';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';

import * as PerfilService from '~/services/perfil';
import * as ExceptionService from '~/services/exception';

import { signOut } from '~/store/modules/auth/actions';

import { Content, User } from './styles';

const validConfirmPassword = ({ value: confirmacaoSenha, _parent }) => {
  if (_parent) {
    const senha = _parent.get('senha').value;

    if (senha !== confirmacaoSenha) {
      return { confirmPassword: true };
    }
  }

  return null;
};

const form = FormBuilder.group({
  login: [{ value: '', disabled: true }],
  senha: ['', [Validators.required, Validators.minLength(6)]],
  confirmacaoSenha: ['', [Validators.required, validConfirmPassword]],
});

export default function Perfil() {
  const { user } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  async function handleSubmit() {
    await PerfilService.update(form.value)
      .then(() => {
        toast.dismiss();
        toast.success(
          'Senha atualizado com sucesso. Em instantes sua sessão será expirada.'
        );
        toast.onChange(e => {
          if (e === 0) {
            dispatch(signOut());
          }
        });
      })
      .catch(error => ExceptionService.handle(error));
  }

  return (
    <Content>
      <FieldGroup
        control={form}
        render={({ invalid }) => (
          <form>
            <User>{user}</User>
            <div className="p-grid p-fluid">
              <div className="p-col-12 p-md-12 p-sm-12">
                <FieldControl name="senha">
                  {({ handler, touched, hasError }) => (
                    <>
                      <Password
                        {...handler()}
                        placeholder="Senha"
                        promptLabel="Informe a senha"
                        weakLabel="Fraca"
                        mediumLabel="Media"
                        strongLabel="Forte"
                      />
                      {touched && hasError('required') && (
                        <Message severity="error" text="Senha obrigatória" />
                      )}
                      {touched && hasError('minLength') && (
                        <Message
                          severity="error"
                          text="Senha deve conter no mínimo 6 caracteres"
                        />
                      )}
                    </>
                  )}
                </FieldControl>
              </div>

              <div className="p-col-12 p-md-12 p-sm-12">
                <FieldControl name="confirmacaoSenha">
                  {({ handler, touched, hasError }) => (
                    <>
                      <Password
                        {...handler()}
                        feedback={false}
                        placeholder="Confirmação de senha"
                      />
                      {touched && hasError('required') && (
                        <Message
                          severity="error"
                          text="Confirmação obrigatória"
                        />
                      )}
                      {touched && hasError('confirmPassword') && (
                        <Message severity="error" text="Senhas não conferem" />
                      )}
                    </>
                  )}
                </FieldControl>
              </div>

              <div className="p-col-12 p-md-12 p-sm-12">
                <Button
                  disabled={invalid}
                  type="button"
                  label="Salvar"
                  onClick={handleSubmit}
                />
              </div>
            </div>
          </form>
        )}
      />
    </Content>
  );
}
