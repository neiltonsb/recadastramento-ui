import React from 'react';
import history from '~/services/history';

import { ErrorCode, ErrorDescription, Button } from './styles';

export default function NoMatch() {
  return (
    <>
      <ErrorCode>404</ErrorCode>
      <ErrorDescription>
        Opaaa desculpe... <br />A página que você está tentando acessar não foi
        encontrada. Se precisar de ajuda, entre em contato conosco.
      </ErrorDescription>
      <Button type="button" onClick={() => history.goBack()}>
        Voltar
      </Button>
    </>
  );
}
