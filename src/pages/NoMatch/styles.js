import styled from 'styled-components';

import { darken } from 'polished';

export const ErrorCode = styled.div`
  font-size: 80px;
  color: #006699;
`;

export const ErrorDescription = styled.div`
  color: #333;
  text-align: center;
  line-height: 2;
`;

export const Button = styled.button`
  margin-top: 10px;

  border: none;
  border-radius: 4px;

  width: 80px;
  height: 40px;

  background: #006699;
  color: #fff;

  &:hover {
    background: ${darken(0.1, '#006699')};
  }
`;
