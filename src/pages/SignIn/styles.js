import styled from 'styled-components';

import { ProgressBar } from 'primereact/progressbar';

import { darken } from 'polished';

export const Container = styled.div`
  border: 1px solid #eee;
  border-radius: 4px;

  div {
    background: #eee;
    text-align: left;
    padding: 10px 20px;
    font-size: 14px;

    color: #006699;
    font-weight: bold;
  }

  form {
    display: flex;
    flex-direction: column;
    padding: 20px;

    input {
      margin: 0 0 10px;
      padding: 0 15px;
      border: 0;
      border-radius: 4px;
      height: 40px;

      border: 1px solid #eee;
      color: #333;
    }

    button {
      height: 40px;
      border: 0;
      border-radius: 4px;
      background: #006699;

      color: #fff;
      font-weight: bold;
      font-size: 14px;

      margin-bottom: 10px;

      &:hover {
        background: ${darken(0.1, '#006699')};
      }

      &:disabled {
        opacity: 0.6;
        pointer-events: none;
      }
    }

    a {
      color: #006699;
      font-size: 14px;

      &:hover {
        color: ${darken(0.1, '#006699')};
      }
    }
  }
`;

export const Error = styled.span`
  font-size: 12px;
  color: #ff4747;
  margin-bottom: 10px;
  text-align: left;
`;

export const Progress = styled(ProgressBar).attrs({
  style: {
    height: '3px',
  },
})`
  padding: 0 !important;
  display: ${props => (props.visible ? 'block' : 'none')};
`;
