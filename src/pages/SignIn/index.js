import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';

import { Container, Error, Progress } from './styles';

import { signInRequest } from '~/store/modules/auth/actions';

const form = FormBuilder.group({
  login: ['', Validators.required],
  password: ['', Validators.required],
});

export default function SignIn() {
  const { loading } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  function handleSubmit(e) {
    e.preventDefault();
    const { login, password } = form.value;
    dispatch(signInRequest(login, password));
  }

  return (
    <Container>
      <div>Autênticação</div>
      <Progress mode="indeterminate" visible={loading} />

      <FieldGroup control={form}>
        {({ invalid }) => (
          <form onSubmit={handleSubmit}>
            <FieldControl name="login">
              {({ handler, touched, hasError }) => (
                <>
                  <input {...handler()} placeholder="Usuário" />
                  {touched && hasError('required') && (
                    <Error>Usuário obrigatório</Error>
                  )}
                </>
              )}
            </FieldControl>
            <FieldControl name="password">
              {({ handler, touched, hasError }) => (
                <>
                  <input {...handler()} type="password" placeholder="Senha" />
                  {touched && hasError('required') && (
                    <Error>Senha obrigatório</Error>
                  )}
                </>
              )}
            </FieldControl>

            <button disabled={invalid} type="submit">
              Acessar
            </button>
            <Link to="/"> Voltar para página inicial</Link>
          </form>
        )}
      </FieldGroup>
    </Container>
  );
}
