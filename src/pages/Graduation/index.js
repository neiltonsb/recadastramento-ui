import React, { useState, useEffect } from 'react';

import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Message } from 'primereact/message';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';
import { toast } from 'react-toastify';
import { confirmAlert } from 'react-confirm-alert';

import * as GraduationService from '~/services/graduation';
import * as ExceptionService from '~/services/exception';

import { Content, Navbar } from './styles';

const form = FormBuilder.group({
  codigo: [''],
  nome: ['', Validators.required],
});

export default function Graduation() {
  const [visibleDialog, setVisibleDialog] = useState(false);
  const [loading, setLoading] = useState(false);

  const [page, setPage] = useState(0);
  const [first, setFirst] = useState(0);
  const [rows, setRows] = useState(20);
  const [totalRecords, setTotalRecords] = useState(0);
  const [filtro, setFiltro] = useState({ nome: '' });
  const [graduations, setGraduations] = useState([]);

  async function loadGraduations() {
    setLoading(true);

    const { content, paginator } = await GraduationService.find({
      page,
      rows,
      filtro,
    }).catch(error => ExceptionService.handle(error));

    setGraduations(content);
    setRows(paginator.rows);
    setTotalRecords(paginator.totalRecords);
    setLoading(false);
  }

  useEffect(() => {
    loadGraduations();
  }, [filtro, page, rows]); // eslint-disable-line

  function onPage(event) {
    setFirst(event.first);
    setPage(event.page);
    setRows(event.rows);
  }

  function onHide() {
    setVisibleDialog(false);
    form.reset();
  }

  function handleEdit(graduation) {
    form.patchValue(graduation);
    setVisibleDialog(true);
  }

  async function handleRemove(graduation) {
    confirmAlert({
      title: 'Confirmação',
      message: 'Deseja realmente excluir?',
      buttons: [
        {
          label: 'Sim',
          onClick: async () => {
            await GraduationService.remove(graduation.codigo)
              .then(() => {
                toast.success('Formação excluída com sucesso!');
                loadGraduations();
              })
              .catch(error => ExceptionService.handle(error));
          },
        },
        {
          label: 'Não',
        },
      ],
    });
  }

  async function handleSubmit() {
    const editando = Boolean(form.get('codigo').value);

    if (editando) {
      await GraduationService.update(form.value)
        .then(({ data: graduation }) => {
          onHide();
          setFiltro({ ...filtro, nome: graduation.nome });
          toast.success('Formação salva com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    } else {
      await GraduationService.create(form.value)
        .then(({ data: graduation }) => {
          onHide();
          setFiltro({ ...filtro, nome: graduation.nome });
          toast.success('Formação salva com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    }
  }

  return (
    <Content>
      <Navbar>
        <h1>Formações</h1>
        <button onClick={() => setVisibleDialog(true)} type="button">
          Adicionar
        </button>
      </Navbar>

      <form onSubmit={e => e.preventDefault()}>
        <div className="p-grid p-fluid">
          <div className="p-col-12 p-md-4 p-sm-12">
            <InputText
              placeholder="Nome"
              value={filtro.nome}
              onChange={e => setFiltro({ ...filtro, nome: e.target.value })}
            />
          </div>
        </div>
      </form>

      <DataTable
        lazy
        paginator
        responsive
        loading={loading}
        first={first}
        rows={rows}
        totalRecords={totalRecords}
        rowsPerPageOptions={[20, 50, 100]}
        value={graduations}
        onPage={onPage}
      >
        <Column field="nome" header="Nome" style={{ textAlign: 'left' }} />

        <Column
          style={{ textAlign: 'center', width: 100 }}
          body={rowData => (
            <div>
              <Button
                style={{ marginRight: 5 }}
                type="button"
                icon="pi pi-pencil"
                onClick={() => handleEdit(rowData)}
              />
              <Button
                type="button"
                icon="pi pi-trash"
                className="p-button-danger"
                onClick={() => handleRemove(rowData)}
              />
            </div>
          )}
        />
      </DataTable>

      <Dialog
        modal
        header="Adicionar"
        style={{ width: 400 }}
        visible={visibleDialog}
        onHide={onHide}
      >
        <FieldGroup
          control={form}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-12 p-sm-12">
                  <FieldControl name="nome">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="60"
                          placeholder="Nome"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Nome obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
              </div>

              <div style={{ textAlign: 'right' }}>
                <Button
                  type="button"
                  disabled={invalid}
                  style={{ marginRight: 10 }}
                  label="Salvar"
                  icon="pi pi-check"
                  onClick={handleSubmit}
                />

                <Button
                  type="button"
                  className="p-button-danger"
                  label="Cancelar"
                  icon="pi pi-times"
                  onClick={onHide}
                />
              </div>
            </form>
          )}
        />
      </Dialog>
    </Content>
  );
}
