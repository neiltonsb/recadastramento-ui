import styled from 'styled-components';
import { ProgressBar } from 'primereact/progressbar';

export const Container = styled.div`
  max-width: 1200px;
  margin: 50px auto;
`;

export const Step = styled.div`
  margin: 0 20px;
  display: ${props => (props.active ? 'block' : 'none')};
`;

export const ConfirmSection = styled.div`
  border-bottom: 1px solid #e3e3e3;
  margin: 10px 0;
  padding: 10px 0;

  label {
    color: #333;
    font-size: 12px;
    font-weight: bold;
  }

  p {
    font-size: 16px;
  }
`;

export const Progress = styled(ProgressBar).attrs({
  style: {
    height: '5px',
  },
})`
  display: ${props => (props.visible ? 'block' : 'none')};
`;
