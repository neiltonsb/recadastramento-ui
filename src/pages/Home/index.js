import React, { useState } from 'react';

import { Steps } from 'primereact/steps';
import { Button } from 'primereact/button';
import { Message } from 'primereact/message';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';

import InputMask from 'react-input-mask';
import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';
import { confirmAlert } from 'react-confirm-alert';

import axios from 'axios';

import api from '~/services/api';
import {
  orgaos,
  ufs,
  classificacoes,
  niveisEscolares,
} from '~/services/static';
import * as Exception from '~/services/exception';
import CargoDropdown from '~/components/CargoDropdown';
import FormacaoDropdown from '~/components/FormacaoDropdown';
import UnidadeGestoraDropdown from '~/components/UnidadeGestoraDropdown';
import FeedbackAlert from '~/components/FeedbackAlert';

import { Container, Step, ConfirmSection, Progress } from './styles';

const form1 = FormBuilder.group({
  cpf: ['', Validators.required],
  nome: [{ value: '', disabled: true }],
  rg: ['', Validators.required],
  orgaoEmissor: ['', Validators.required],
  ufEmissor: ['', Validators.required],
  email: ['', [Validators.required, Validators.email]],
  telefoneCorporativo: [
    '',
    [
      Validators.required,
      Validators.pattern(/(?:\()[0-9]{2}(?:\))\s?[0-9]{4,5}(?:-)[0-9]{4}$/),
    ],
  ],
  telefoneCelular: [
    '',
    Validators.pattern(/(?:\()[0-9]{2}(?:\))\s?[0-9]{4,5}(?:-)[0-9]{4}$/),
  ],
});

const form2 = FormBuilder.group({
  cep: ['', Validators.required],
  logradouro: ['', Validators.required],
  numero: ['', Validators.required],
  complemento: [''],
  bairro: ['', Validators.required],
  localidade: ['', Validators.required],
  uf: ['', Validators.required],
});

const form3 = FormBuilder.group({
  matricula: [],
  classificacao: [],
  anoNomeacao: ['', [Validators.required, Validators.pattern(/([0-9]{4})/)]],
  cargo: [null, Validators.required],
  nivelEscolar: [null, Validators.required],
  formacao: [null, Validators.required],
  unidadeGestoraOrigem: [null, Validators.required],
  unidadeGestoraAtual: [null, Validators.required],
});

export default function Home() {
  const [activeIndex, setActiveIndex] = useState(0);
  const [activeProgress, setActiveProgress] = useState(false);

  const items = [
    {
      label: 'Passo 1',
      command: () => {
        setActiveIndex(0);
      },
    },
    {
      label: 'Passo 2',
      command: () => {
        setActiveIndex(1);
      },
    },
    {
      label: 'Passo 3',
      command: () => {
        setActiveIndex(2);
      },
    },
    {
      label: 'Confirmação',
      command: () => {
        setActiveIndex(3);
      },
    },
  ];

  async function handleCpf(value) {
    const patt = new RegExp('([0-9]{3}).([0-9]{3}).([0-9]{3})-([0-9]{2})');

    if (patt.test(value)) {
      const { data: servidor } = await api.get(`servidores/${value}`);

      if (!servidor) {
        form1.get('cpf').setErrors({ servidorNaoEncontrado: true });
      } else if (!servidor.habilitado) {
        form1.get('cpf').setErrors({ servidorDesabilitado: true });
      } else if (servidor.atualizado) {
        form1.get('cpf').setErrors({ servidorAtualizado: true });
      } else {
        form1.get('nome').setValue(servidor.nome);
        form3.get('matricula').setValue(servidor.matricula);
        form3.get('classificacao').setValue(servidor.classificacao);
      }
    } else {
      form1.get('cpf').setErrors({ cpfInvalido: true });
    }
  }

  async function handleCep(value) {
    const cep = value
      .replace('.', '')
      .replace('-', '')
      .replace('_', '');

    if (cep.length === 8) {
      const { data: endereco } = await axios.get(
        `https://viacep.com.br/ws/${cep}/json`
      );

      form2.patchValue(endereco);
    } else {
      form2.get('cep').setErrors({ cepInvalido: true });
    }
  }

  async function handleSubmit() {
    setActiveProgress(true);
    const endereco = {
      ...form2.value,
    };
    const funcional = {
      ...form3.value,
      nivelEscolar: form3.value.nivelEscolar.enum,
    };
    const servidor = {
      ...form1.value,
      endereco,
      funcional,
    };
    await api
      .post('servidores_atualizados', servidor)
      .then(() => {
        confirmAlert({
          customUI: ({ onClose }) => {
            return (
              <FeedbackAlert
                onClose={onClose}
                message="Recadastramento realizado com sucesso. Em breve você receberá um e-mail com a confirmação deste processo."
              />
            );
          },
        });
        form1.reset();
        form2.reset();
        form3.reset();
        setActiveIndex(0);
      })
      .catch(error => {
        Exception.handle(error);
      })
      .finally(() => {
        setActiveProgress(false);
      });
  }

  return (
    <Container id="recadastramento">
      <Steps readOnly model={items} activeIndex={activeIndex} />
      <Step active={activeIndex === 0}>
        <FieldGroup
          control={form1}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="cpf">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputMask
                          {...handler()}
                          className="p-inputtext"
                          mask="999.999.999-99"
                          placeholder="CPF"
                          onBlur={e => {
                            handler().onBlur(e);
                            handleCpf(e.target.value);
                          }}
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="CPF obrigatório" />
                        )}
                        {touched && hasError('cpfInvalido') && (
                          <Message severity="error" text="CPF inválido" />
                        )}
                        {touched && hasError('servidorDesabilitado') && (
                          <Message
                            severity="error"
                            text="CPF não habilitado para o recadastramento"
                          />
                        )}
                        {touched && hasError('servidorNaoEncontrado') && (
                          <Message
                            severity="error"
                            text="CPF não encontrado em nossa base de dados"
                          />
                        )}
                        {touched && hasError('servidorAtualizado') && (
                          <Message
                            severity="error"
                            text="Servidor já realizou o recadastramento"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-8 p-sm-12">
                  <FieldControl name="nome">
                    {({ handler }) => (
                      <InputText {...handler()} placeholder="Nome" />
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="rg">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          placeholder="RG"
                          maxLength="20"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="RG obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="orgaoEmissor">
                    {({ handler, touched, hasError }) => (
                      <>
                        <Dropdown
                          {...handler()}
                          placeholder="Orgão emissor"
                          options={orgaos}
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="Orgão emissor obrigatório"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="ufEmissor">
                    {({ handler, touched, hasError }) => (
                      <>
                        <Dropdown
                          {...handler()}
                          placeholder="UF"
                          options={ufs}
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="UF obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="email">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          placeholder="E-mail"
                          maxLength="50"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="E-mail obrigatório" />
                        )}
                        {touched && hasError('email') && (
                          <Message severity="error" text="E-mail inválido" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="telefoneCorporativo">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputMask
                          {...handler()}
                          mask="(99) 9999-9999"
                          className="p-inputtext"
                          placeholder="Telefone corporativo"
                          onBlur={e => {
                            handler().onBlur(e);
                          }}
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="Telefone corporativo obrigatório"
                          />
                        )}
                        {touched && hasError('pattern') && (
                          <Message
                            severity="error"
                            text="Telefone corporativo inválido"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="telefoneCelular">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputMask
                          {...handler()}
                          mask="(99) 99999-9999"
                          className="p-inputtext"
                          placeholder="Telefone celular"
                          onBlur={e => {
                            handler().onBlur(e);
                          }}
                        />
                        {touched && hasError('pattern') && (
                          <Message
                            severity="error"
                            text="Telefone celular inválido"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
              </div>

              <Button
                disabled={invalid}
                label="Próximo"
                type="button"
                onClick={() => setActiveIndex(1)}
              />
            </form>
          )}
        />
      </Step>
      <Step active={activeIndex === 1}>
        <FieldGroup
          control={form2}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="cep">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputMask
                          {...handler()}
                          className="p-inputtext"
                          mask="99.999-999"
                          placeholder="CEP"
                          onBlur={e => {
                            handler().onBlur(e);
                            handleCep(e.target.value);
                          }}
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="CEP obrigatório" />
                        )}
                        {touched && hasError('cepInvalido') && (
                          <Message severity="error" text="CEP inválido" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-8 p-sm-12">
                  <FieldControl name="logradouro">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          placeholder="Logradouro"
                          maxLength="50"
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="Logradouro obrigatório"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="numero">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          placeholder="Nº"
                          maxLength="10"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Nº obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="complemento">
                    {({ handler }) => (
                      <InputText
                        {...handler()}
                        placeholder="Complemento"
                        maxLength="30"
                      />
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="bairro">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          placeholder="Bairro"
                          maxLength="50"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Bairro obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="localidade">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          placeholder="Cidade"
                          maxLength="50"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Cidade obrigatória" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="uf">
                    {({ handler, touched, hasError }) => (
                      <>
                        <Dropdown
                          {...handler()}
                          placeholder="UF"
                          options={ufs}
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="UF obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
              </div>

              <Button
                style={{ marginRight: 5 }}
                icon="pi pi-arrow-left"
                className="p-button-secondary"
                label="Voltar"
                type="button"
                onClick={() => setActiveIndex(0)}
              />

              <Button
                disabled={invalid}
                label="Próximo"
                type="button"
                onClick={() => setActiveIndex(2)}
              />
            </form>
          )}
        />
      </Step>
      <Step active={activeIndex === 2}>
        <FieldGroup
          control={form3}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="matricula">
                    {({ handler }) => (
                      <InputText
                        {...handler()}
                        placeholder="Matricula"
                        maxLength="20"
                      />
                    )}
                  </FieldControl>
                </div>
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="classificacao">
                    {({ handler }) => (
                      <Dropdown
                        {...handler()}
                        placeholder="Classificação"
                        options={classificacoes}
                      />
                    )}
                  </FieldControl>
                </div>
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="anoNomeacao">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          placeholder="Ano de nomeação"
                          maxLength="4"
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="Ano de nomeação obrigatório"
                          />
                        )}
                        {touched && hasError('pattern') && (
                          <Message
                            severity="error"
                            text="Ano de nomeação inválido"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="cargo">
                    {({ handler, touched, hasError }) => (
                      <>
                        <CargoDropdown
                          {...handler()}
                          placeholder="Cargo"
                          filter
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="CEP obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="nivelEscolar">
                    {({ handler, touched, hasError }) => (
                      <>
                        <Dropdown
                          {...handler()}
                          placeholder="Nível escolar"
                          options={niveisEscolares}
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="Nível escolar obrigatório"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="formacao">
                    {({ handler, touched, hasError }) => (
                      <>
                        <FormacaoDropdown
                          {...handler()}
                          placeholder="Formação"
                          filter
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="Formação obrigatória"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="unidadeGestoraOrigem">
                    {({ handler, touched, hasError }) => (
                      <>
                        <UnidadeGestoraDropdown
                          {...handler()}
                          placeholder="UG de origem"
                          filter
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="UG de origem obrigatória"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="unidadeGestoraAtual">
                    {({ handler, touched, hasError }) => (
                      <>
                        <UnidadeGestoraDropdown
                          {...handler()}
                          placeholder="UG atual"
                          filter
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="UG atual obrigatória"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
              </div>

              <Button
                style={{ marginRight: 5 }}
                icon="pi pi-arrow-left"
                className="p-button-secondary"
                label="Voltar"
                type="button"
                onClick={() => setActiveIndex(1)}
              />

              <Button
                disabled={invalid}
                label="Próximo"
                type="button"
                onClick={() => setActiveIndex(3)}
              />
            </form>
          )}
        />
      </Step>
      <Step active={activeIndex === 3}>
        <div className="p-grid p-fluid">
          <div className="p-col-12 p-md-12 p-sm-12">
            <Progress mode="indeterminate" visible={activeProgress} />
          </div>
        </div>
        <ConfirmSection className="p-grid p-fluid">
          <div className="p-col-12 p-md-2 p-sm-12">
            <label>CPF</label>
            <p>{form1.get('cpf').value}</p>
          </div>

          <div className="p-col-12 p-md-8 p-sm-12">
            <label>Nome</label>
            <p>{form1.get('nome').value}</p>
          </div>

          <div className="p-col-12 p-md-2 p-sm-12">
            <label>RG</label>
            <p>
              {form1.get('rg').value} {form1.get('orgaoEmissor').value}/
              {form1.get('ufEmissor').value}
            </p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>E-mail</label>
            <p>{form1.get('email').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Telefone corporativo</label>
            <p>{form1.get('telefoneCorporativo').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Telefone celular</label>
            <p>{form1.get('telefoneCelular').value}</p>
          </div>
        </ConfirmSection>

        <ConfirmSection className="p-grid p-fluid">
          <div className="p-col-12 p-md-4 p-sm-12">
            <label>CEP</label>
            <p>{form2.get('cep').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Lograoduro</label>
            <p>{form2.get('logradouro').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Nº</label>
            <p>{form2.get('numero').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Complemento</label>
            <p>{form2.get('complemento').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Bairro</label>
            <p>{form2.get('bairro').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Cidade/UF</label>
            <p>
              {form2.get('localidade').value}/{form2.get('uf').value}
            </p>
          </div>
        </ConfirmSection>

        <ConfirmSection className="p-grid p-fluid">
          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Matricula</label>
            <p>{form3.get('matricula').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Classificação</label>
            <p>{form3.get('classificacao').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Ano de nomeação</label>
            <p>{form3.get('anoNomeacao').value}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Cargo</label>
            <p>{form3.get('cargo').value && form3.get('cargo').value.nome}</p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Nível escolar</label>
            <p>
              {form3.get('nivelEscolar').value &&
                form3.get('nivelEscolar').value.descricao}
            </p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>Formação</label>
            <p>
              {form3.get('formacao').value && form3.get('formacao').value.nome}
            </p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>UG de origem</label>
            <p>
              {form3.get('unidadeGestoraOrigem').value &&
                `${form3.get('unidadeGestoraOrigem').value.codigoUg} - ${
                  form3.get('unidadeGestoraOrigem').value.nome
                }`}
            </p>
          </div>

          <div className="p-col-12 p-md-4 p-sm-12">
            <label>UG atual</label>
            <p>
              {form3.get('unidadeGestoraAtual').value &&
                `${form3.get('unidadeGestoraAtual').value.codigoUg} - ${
                  form3.get('unidadeGestoraAtual').value.nome
                }`}
            </p>
          </div>
        </ConfirmSection>

        <Button
          style={{ marginRight: 5 }}
          icon="pi pi-arrow-left"
          className="p-button-secondary"
          label="Voltar"
          type="button"
          onClick={() => setActiveIndex(2)}
        />

        <Button
          className="p-button-success"
          label="Finalizar"
          onClick={handleSubmit}
        />
      </Step>
    </Container>
  );
}
