import React, { useState, useEffect } from 'react';

import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Message } from 'primereact/message';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';
import { toast } from 'react-toastify';
import { confirmAlert } from 'react-confirm-alert';

import * as WorkService from '~/services/work';
import * as ExceptionService from '~/services/exception';

import { Content, Navbar } from './styles';

const form = FormBuilder.group({
  codigo: [''],
  nome: ['', Validators.required],
});

export default function Work() {
  const [visibleDialog, setVisibleDialog] = useState(false);
  const [loading, setLoading] = useState(false);

  const [page, setPage] = useState(0);
  const [first, setFirst] = useState(0);
  const [rows, setRows] = useState(20);
  const [totalRecords, setTotalRecords] = useState(0);
  const [filtro, setFiltro] = useState({ nome: '' });
  const [works, setWorks] = useState([]);

  async function loadWorks() {
    setLoading(true);

    const { content, paginator } = await WorkService.find({
      page,
      rows,
      filtro,
    }).catch(error => ExceptionService.handle(error));

    setWorks(content);
    setRows(paginator.rows);
    setTotalRecords(paginator.totalRecords);
    setLoading(false);
  }

  useEffect(() => {
    loadWorks();
  }, [filtro, page, rows]); // eslint-disable-line

  function onPage(event) {
    setFirst(event.first);
    setPage(event.page);
    setRows(event.rows);
  }

  function onHide() {
    setVisibleDialog(false);
    form.reset();
  }

  function handleEdit(work) {
    form.patchValue(work);
    setVisibleDialog(true);
  }

  async function handleRemove(work) {
    confirmAlert({
      title: 'Confirmação',
      message: 'Deseja realmente excluir?',
      buttons: [
        {
          label: 'Sim',
          onClick: async () => {
            await WorkService.remove(work.codigo)
              .then(() => {
                toast.success('Cargo excluído com sucesso!');
                loadWorks();
              })
              .catch(error => ExceptionService.handle(error));
          },
        },
        {
          label: 'Não',
        },
      ],
    });
  }

  async function handleSubmit() {
    const editando = Boolean(form.get('codigo').value);

    if (editando) {
      await WorkService.update(form.value)
        .then(({ data: work }) => {
          onHide();
          setFiltro({ ...filtro, nome: work.nome });
          toast.success('Cargo salvo com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    } else {
      await WorkService.create(form.value)
        .then(({ data: work }) => {
          onHide();
          setFiltro({ ...filtro, nome: work.nome });
          toast.success('Cargo salvo com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    }
  }

  return (
    <Content>
      <Navbar>
        <h1>Cargos</h1>
        <button onClick={() => setVisibleDialog(true)} type="button">
          Adicionar
        </button>
      </Navbar>

      <form onSubmit={e => e.preventDefault()}>
        <div className="p-grid p-fluid">
          <div className="p-col-12 p-md-4 p-sm-12">
            <InputText
              placeholder="Nome"
              value={filtro.nome}
              onChange={e => setFiltro({ ...filtro, nome: e.target.value })}
            />
          </div>
        </div>
      </form>

      <DataTable
        lazy
        paginator
        responsive
        loading={loading}
        first={first}
        rows={rows}
        totalRecords={totalRecords}
        rowsPerPageOptions={[20, 50, 100]}
        value={works}
        onPage={onPage}
      >
        <Column field="nome" header="Nome" style={{ textAlign: 'left' }} />

        <Column
          style={{ textAlign: 'center', width: 100 }}
          body={rowData => (
            <div>
              <Button
                style={{ marginRight: 5 }}
                type="button"
                icon="pi pi-pencil"
                onClick={() => handleEdit(rowData)}
              />
              <Button
                type="button"
                icon="pi pi-trash"
                className="p-button-danger"
                onClick={() => handleRemove(rowData)}
              />
            </div>
          )}
        />
      </DataTable>

      <Dialog
        modal
        header="Adicionar"
        style={{ width: 400 }}
        visible={visibleDialog}
        onHide={onHide}
      >
        <FieldGroup
          control={form}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-12 p-sm-12">
                  <FieldControl name="nome">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="40"
                          placeholder="Nome"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Nome obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>
              </div>

              <div style={{ textAlign: 'right' }}>
                <Button
                  type="button"
                  disabled={invalid}
                  style={{ marginRight: 10 }}
                  label="Salvar"
                  icon="pi pi-check"
                  onClick={handleSubmit}
                />

                <Button
                  type="button"
                  className="p-button-danger"
                  label="Cancelar"
                  icon="pi pi-times"
                  onClick={onHide}
                />
              </div>
            </form>
          )}
        />
      </Dialog>
    </Content>
  );
}
