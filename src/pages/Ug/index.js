import React, { useState, useEffect } from 'react';

import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Message } from 'primereact/message';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputMask } from 'primereact/inputmask';
import { ToggleButton } from 'primereact/togglebutton';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';
import { toast } from 'react-toastify';
import { confirmAlert } from 'react-confirm-alert';

import { Content, Navbar } from './styles';

import * as UgService from '~/services/ug';
import * as ExceptionService from '~/services/exception';

const form = FormBuilder.group({
  codigo: [''],
  codigoUg: ['', Validators.required],
  nome: ['', Validators.required],
  nomeContador: [''],
  emailContador: ['', Validators.email],
  telefoneContador: [''],
  ativo: [true],
});

export default function Ug() {
  const [visibleDialog, setVisibleDialog] = useState(false);
  const [loading, setLoading] = useState(false);

  const [filtro, setFiltro] = useState({ codigoUg: '', nome: '' });
  const [page, setPage] = useState(0);
  const [first, setFirst] = useState(0);
  const [rows, setRows] = useState(20);
  const [totalRecords, setTotalRecords] = useState(0);
  const [ugs, setUgs] = useState([]);

  async function loadUgs() {
    setLoading(true);

    const { content, paginator } = await UgService.find({
      page,
      rows,
      filtro,
    }).catch(error => ExceptionService.handle(error));

    setUgs(content);
    setRows(paginator.rows);
    setTotalRecords(paginator.totalRecords);
    setLoading(false);
  }

  useEffect(() => {
    loadUgs();
  }, [filtro, page, rows]); // eslint-disable-line

  function onPage(event) {
    setFirst(event.first);
    setPage(event.page);
    setRows(event.rows);
  }

  function onHide() {
    setVisibleDialog(false);
    form.reset();
  }

  function handleEdit(ug) {
    form.patchValue(ug);
    setVisibleDialog(true);
  }

  async function handleSubmit() {
    const editando = Boolean(form.get('codigo').value);

    if (editando) {
      await UgService.update(form.value)
        .then(({ data: ug }) => {
          onHide();
          setFiltro({ ...filtro, codigoUg: ug.codigoUg });
          toast.success('Unidade gestora salva com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    } else {
      await UgService.create(form.value)
        .then(({ data: ug }) => {
          onHide();
          setFiltro({ ...filtro, codigoUg: ug.codigoUg });
          toast.success('Unidade gestora salva com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    }
  }

  async function handleRemove(ug) {
    confirmAlert({
      title: 'Confirmação',
      message: 'Deseja realmente excluir?',
      buttons: [
        {
          label: 'Sim',
          onClick: async () => {
            await UgService.remove(ug.codigo)
              .then(() => {
                toast.success('Unidade Gestora excluída com sucesso!');
                loadUgs();
              })
              .catch(error => ExceptionService.handle(error));
          },
        },
        {
          label: 'Não',
        },
      ],
    });
  }

  return (
    <Content>
      <Navbar>
        <h1>Unidades Gestoras</h1>
        <button onClick={() => setVisibleDialog(true)} type="button">
          Adicionar
        </button>
      </Navbar>

      <form onSubmit={e => e.preventDefault()}>
        <div className="p-grid p-fluid">
          <div className="p-col-12 p-md-4 p-sm-12">
            <InputText
              placeholder="Código"
              value={filtro.codigoUg}
              onChange={e => setFiltro({ ...filtro, codigoUg: e.target.value })}
            />
          </div>

          <div className="p-col-12 p-md-8 p-sm-12">
            <InputText
              placeholder="Nome"
              value={filtro.nome}
              onChange={e => setFiltro({ ...filtro, nome: e.target.value })}
            />
          </div>
        </div>
      </form>

      <DataTable
        lazy
        paginator
        responsive
        loading={loading}
        first={first}
        rows={rows}
        totalRecords={totalRecords}
        rowsPerPageOptions={[20, 50, 100]}
        value={ugs}
        onPage={onPage}
      >
        <Column
          field="codigoUg"
          header="Código"
          style={{ textAlign: 'center', width: '20%' }}
        />
        <Column field="nome" header="Nome" style={{ textAlign: 'left' }} />

        <Column
          style={{ textAlign: 'center', width: 100 }}
          body={rowData => (
            <div>
              <Button
                style={{ marginRight: 5 }}
                type="button"
                icon="pi pi-pencil"
                onClick={() => handleEdit(rowData)}
              />
              <Button
                type="button"
                icon="pi pi-trash"
                className="p-button-danger"
                onClick={() => handleRemove(rowData)}
              />
            </div>
          )}
        />
      </DataTable>

      <Dialog
        modal
        header="Adicionar"
        style={{ width: 'auto' }}
        visible={visibleDialog}
        onHide={onHide}
      >
        <FieldGroup
          control={form}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="codigoUg">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="6"
                          placeholder="Código"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Código obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-8 p-sm-12">
                  <FieldControl name="nome">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="50"
                          placeholder="Nome"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Nome obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="nomeContador">
                    {({ handler }) => (
                      <InputText
                        {...handler()}
                        maxLength="50"
                        placeholder="Nome contador"
                      />
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="emailContador">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="50"
                          placeholder="E-mail"
                        />
                        {touched && hasError('email') && (
                          <Message severity="error" text="E-mail inválido" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="telefoneContador">
                    {({ handler }) => (
                      <InputMask
                        {...handler()}
                        mask="(99) 9999-9999"
                        placeholder="Telefone"
                      />
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-2 p-sm-12">
                  <FieldControl name="ativo">
                    {({ handler, value }) => (
                      <>
                        <ToggleButton
                          {...handler()}
                          checked={value}
                          onLabel="Ativo"
                          offLabel="Inativo"
                          onIcon="pi pi-check"
                          offIcon="pi pi-times"
                        />
                      </>
                    )}
                  </FieldControl>
                </div>
              </div>

              <div style={{ textAlign: 'right' }}>
                <Button
                  type="button"
                  disabled={invalid}
                  style={{ marginRight: 10 }}
                  label="Salvar"
                  icon="pi pi-check"
                  onClick={handleSubmit}
                />

                <Button
                  type="button"
                  className="p-button-danger"
                  label="Cancelar"
                  icon="pi pi-times"
                  onClick={onHide}
                />
              </div>
            </form>
          )}
        />
      </Dialog>
    </Content>
  );
}
