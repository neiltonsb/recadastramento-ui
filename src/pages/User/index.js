import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Message } from 'primereact/message';
import { Password } from 'primereact/password';
import { ToggleButton } from 'primereact/togglebutton';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';
import { toast } from 'react-toastify';
import { confirmAlert } from 'react-confirm-alert';

import UnidadeGestoraDropdown from '~/components/UnidadeGestoraDropdown';
import GrupoMultiSelect from '~/components/GrupoMultiSelect';

import * as UsuarioService from '~/services/usuario';
import * as ExceptionService from '~/services/exception';

import { Content, Navbar } from './styles';

const form = FormBuilder.group({
  codigo: [],
  login: ['', Validators.required],
  senha: [''],
  confirmacaoSenha: [''],
  ativo: [true],
  unidadeGestora: [{}, Validators.required],
  grupos: [[], Validators.required],
});

export default function User() {
  const { user_id } = useSelector(state => state.auth);

  const [visibleDialog, setVisibleDialog] = useState(false);
  const [loading, setLoading] = useState(false);

  const [page, setPage] = useState(0);
  const [first, setFirst] = useState(0);
  const [rows, setRows] = useState(20);
  const [totalRecords, setTotalRecords] = useState(0);
  const [filtro, setFiltro] = useState({ login: '' });

  const [usuarios, setUsuarios] = useState([]);

  async function loadUsuarios() {
    setLoading(true);

    const { content, paginator } = await UsuarioService.find({
      page,
      rows,
      filtro,
    }).catch(error => ExceptionService.handle(error));

    const users = content.filter(u => u.codigo !== user_id);

    setUsuarios(users);
    setRows(paginator.rows);
    setTotalRecords(paginator.totalRecords);

    setLoading(false);
  }

  useEffect(() => {loadUsuarios()}, [filtro, page, rows]) // eslint-disable-line

  function onPage(event) {
    setFirst(event.first);
    setPage(event.page);
    setRows(event.rows);
  }

  function onHide() {
    setVisibleDialog(false);
    form.reset({ grupos: [] });
  }

  async function handleSubmit() {
    const editando = Boolean(form.get('codigo').value);
    if (editando) {
      await UsuarioService.update(form.value)
        .then(({ data: usuario }) => {
          onHide();
          setFiltro({ ...filtro, login: usuario.login });
          toast.success('Usuário salvo com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    } else {
      await UsuarioService.create(form.value)
        .then(({ data: usuario }) => {
          onHide();
          setFiltro({ ...filtro, login: usuario.login });
          toast.success('Usuário salvo com sucesso!');
        })
        .catch(error => ExceptionService.handle(error));
    }
  }

  function handleEdit(usuario) {
    const grupos = usuario.grupos.map(g => ({
      codigo: g.codigo,
    }));
    const unidadeGestora = {
      codigo: usuario.unidadeGestora.codigo,
      codigoUg: usuario.unidadeGestora.codigoUg,
      nome: usuario.unidadeGestora.nome,
    };

    const usr = {
      ...usuario,
      grupos,
      unidadeGestora,
    };

    form.patchValue(usr);
    setVisibleDialog(true);
  }

  function handleRemove(usuario) {
    confirmAlert({
      title: 'Confirmação',
      message: 'Deseja realmente excluir?',
      buttons: [
        {
          label: 'Sim',
          onClick: async () => {
            await UsuarioService.remove(usuario.codigo)
              .then(() => {
                toast.success('Usuário excluído com sucesso!');
                loadUsuarios();
              })
              .catch(error => ExceptionService.handle(error));
          },
        },
        {
          label: 'Não',
        },
      ],
    });
  }

  return (
    <Content>
      <Navbar>
        <h1>Usuários</h1>
        <button onClick={() => setVisibleDialog(true)} type="button">
          Adicionar
        </button>
      </Navbar>

      <form onSubmit={e => e.preventDefault()}>
        <div className="p-grid p-fluid">
          <div className="p-col-12 p-md-4 p-sm-12">
            <InputText
              placeholder="Login"
              value={filtro.login}
              onChange={e => setFiltro({ ...filtro, login: e.target.value })}
            />
          </div>
        </div>
      </form>

      <DataTable
        lazy
        paginator
        responsive
        loading={loading}
        first={first}
        rows={rows}
        totalRecords={totalRecords}
        rowsPerPageOptions={[20, 50, 100]}
        value={usuarios}
        onPage={onPage}
      >
        <Column field="login" header="Login" style={{ textAlign: 'left' }} />
        <Column
          header="UG"
          style={{ textAlign: 'center' }}
          body={rowData => (
            <>
              {rowData.unidadeGestora.codigoUg} - {rowData.unidadeGestora.nome}
            </>
          )}
        />
        <Column
          header="Status"
          style={{ textAlign: 'center' }}
          body={rowData => (
            <span
              className={
                rowData.ativo ? 'badge badge-success' : 'badge badge-danger'
              }
            >
              {rowData.ativo ? 'Ativo' : 'Inativo'}
            </span>
          )}
        />

        <Column
          style={{ textAlign: 'center', width: 100 }}
          body={rowData => (
            <div>
              <Button
                style={{ marginRight: 5 }}
                type="button"
                icon="pi pi-pencil"
                onClick={() => handleEdit(rowData)}
              />
              {rowData.codigo !== user_id && (
                <Button
                  type="button"
                  icon="pi pi-trash"
                  className="p-button-danger"
                  onClick={() => handleRemove(rowData)}
                />
              )}
            </div>
          )}
        />
      </DataTable>

      <Dialog
        modal
        header="Adicionar"
        style={{ width: 'auto' }}
        visible={visibleDialog}
        onHide={onHide}
      >
        <FieldGroup
          control={form}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-12 p-sm-12">
                  <FieldControl name="login">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="6"
                          placeholder="Login"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Login obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-6 p-sm-12">
                  <FieldControl name="senha">
                    {({ handler, touched, hasError }) => (
                      <>
                        <Password
                          {...handler()}
                          placeholder="Senha"
                          promptLabel="Informe a senha"
                          weakLabel="Fraca"
                          mediumLabel="Media"
                          strongLabel="Forte"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Senha obrigatória" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-6 p-sm-12">
                  <FieldControl name="confirmacaoSenha">
                    {({ handler, touched, hasError }) => (
                      <>
                        <Password
                          {...handler()}
                          feedback={false}
                          placeholder="Confirmação de senha"
                        />
                        {touched && hasError('required') && (
                          <Message
                            severity="error"
                            text="Confirmação obrigatória"
                          />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-12 p-sm-12">
                  <FieldControl name="unidadeGestora">
                    {({ handler }) => (
                      <UnidadeGestoraDropdown
                        {...handler()}
                        filter
                        placeholder="Unidade Gestora"
                      />
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-12 p-sm-12">
                  <FieldControl name="grupos">
                    {({ handler }) => (
                      <GrupoMultiSelect {...handler()} placeholder="Grupos" />
                    )}
                  </FieldControl>
                </div>
                <div className="p-col-12 p-md-2 p-sm-12">
                  <FieldControl name="ativo">
                    {({ handler, value }) => (
                      <>
                        <ToggleButton
                          {...handler()}
                          checked={value}
                          onLabel="Ativo"
                          offLabel="Inativo"
                          onIcon="pi pi-check"
                          offIcon="pi pi-times"
                        />
                      </>
                    )}
                  </FieldControl>
                </div>
              </div>

              <div style={{ textAlign: 'right' }}>
                <Button
                  type="button"
                  disabled={invalid}
                  style={{ marginRight: 10 }}
                  label="Salvar"
                  icon="pi pi-check"
                  onClick={handleSubmit}
                />

                <Button
                  type="button"
                  className="p-button-danger"
                  label="Cancelar"
                  icon="pi pi-times"
                  onClick={onHide}
                />
              </div>
            </form>
          )}
        />
      </Dialog>
    </Content>
  );
}
