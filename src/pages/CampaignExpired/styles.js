import styled from 'styled-components';

import { darken } from 'polished';

export const Title = styled.div`
  font-size: 40px;
  color: #006699;
  margin-bottom: 20px;
`;

export const Description = styled.div`
  color: #333;
  text-align: center;
  line-height: 2;
  margin-top: 20px;
`;

export const Button = styled.button`
  margin-top: 10px;

  border: none;
  border-radius: 4px;

  width: 100px;
  height: 40px;

  background: #006699;
  color: #fff;

  &:hover {
    background: ${darken(0.1, '#006699')};
  }
`;
