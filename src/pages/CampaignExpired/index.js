import React, { useState, useEffect } from 'react';

import { FaCalendarTimes } from 'react-icons/fa';

import * as CampaignService from '~/services/campaign';

import { Title, Description, Button } from './styles';

export default function CampaignExpired() {
  const [expirationDate, setExpirationDate] = useState();

  useEffect(() => {
    async function loadExpirationDate() {
      const date = await CampaignService.getExpirationDate();
      setExpirationDate(date);
    }

    loadExpirationDate();
  });

  return (
    <>
      <Title>Campanha encerrada</Title>
      <div>
        <FaCalendarTimes size={80} color="#006699" />
      </div>
      <Description>
        A campanha de recadastramento foi encerrada em {expirationDate}. <br />A
        habilitação de novos servidores está restrita exclusivamente a
        Superitendência de Contabilidade - SUPER.
      </Description>
      <Button
        type="button"
        onClick={() => {
          window.location = process.env.REACT_APP_ROUTER_BASE || '/';
        }}
      >
        Página inicial
      </Button>
    </>
  );
}
