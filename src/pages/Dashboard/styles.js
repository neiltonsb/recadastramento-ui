import styled from 'styled-components';
import { darken } from 'polished';

export const Content = styled.div`
  margin: 20px 0;

  > form {
    margin: 20px 0;
  }
`;

export const Navbar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  * {
    font-family: 'Roboto', monospace, sans-serif;
  }

  h1 {
    color: #333;
  }

  button {
    width: 85px;
    height: 40px;
    border-radius: 4px;
    border: 0;
    background: #006699;
    color: #fff;
    font-weight: bold;

    &:hover {
      background: ${darken(0.1, '#006699')};
    }
  }
`;
