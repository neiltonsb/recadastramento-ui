import React, { useEffect, useState } from 'react';
import { parseISO, format } from 'date-fns';

import { SelectButton } from 'primereact/selectbutton';
import { InputMask } from 'primereact/inputmask';
import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Dropdown } from 'primereact/dropdown';
import { Message } from 'primereact/message';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Messages } from 'primereact/messages';

import * as RInputMask from 'react-input-mask';
import validarCpf from 'validar-cpf';
import { toast } from 'react-toastify';
import { confirmAlert } from 'react-confirm-alert';

import {
  FormBuilder,
  Validators,
  FieldGroup,
  FieldControl,
} from 'react-reactive-form';

import UnidadeGestoraDropdown from '~/components/UnidadeGestoraDropdown';
import UnidadeGestoraMultiSelect from '~/components/UnidadeGestoraMultiSelect';

import { Content, Navbar } from './styles';
import ConfirmAlert from '~/components/ConfirmAlert';

import { classificacoes } from '~/services/static';
import * as ServidorService from '~/services/servidor';
import * as ExceptionService from '~/services/exception';
import * as SecurityService from '~/services/security';

const form = FormBuilder.group({
  cpf: ['', Validators.required],
  nome: ['', Validators.required],
  matricula: [''],
  classificacao: [null],
  email: ['', [Validators.required, Validators.email]],
  unidadeGestora: [null],
});

let messages = {};

export default function Dashboard() {
  const [page, setPage] = useState(0);
  const [first, setFirst] = useState(0);
  const [rows, setRows] = useState(20);
  const [totalRecords, setTotalRecords] = useState(0);
  const [filtro, setFiltro] = useState({
    cpf: '',
    nome: '',
    status: '',
    unidadesGestoras: [],
  });
  const [servidores, setServidores] = useState([]);

  const [loading, setLoading] = useState(false);
  const [visibleDialog, setVisibleDialog] = useState(false);

  async function loadServidores() {
    setLoading(true);

    const { content, paginator } = await ServidorService.find({
      page,
      rows,
      filtro,
    });

    const data = content.map(s => ({
      ...s,
      dataHabilitacao: format(parseISO(s.dataHabilitacao), 'dd/MM/yyyy'),
      dataHoraAtualizacao:
        s.dataHoraAtualizacao &&
        format(parseISO(s.dataHoraAtualizacao), 'dd/MM/yyyy HH:mm:ss'),
    }));
    setServidores(data);
    setRows(paginator.rows);
    setTotalRecords(paginator.totalRecords);

    setLoading(false);
  }

  useEffect(() => {
    loadServidores();
  }, [filtro, page, rows]); // eslint-disable-line

  function onPage(event) {
    setFirst(event.first);
    setPage(event.page);
    setRows(event.rows);
  }

  function onHide() {
    setVisibleDialog(false);
    form.reset();
  }

  async function handleCpf(cpf) {
    messages.clear();
    if (!validarCpf(cpf)) {
      form.get('cpf').setErrors({ cpfInvalido: true });
    } else {
      const { data: servidor } = await ServidorService.findByCpf(cpf);

      if (servidor) {
        if (servidor.habilitado) {
          form.get('cpf').setErrors({ cpfHabilitado: true });
          const msg =
            'Caro usuário, o CPF informado já consta habilitado no processo de Recadastramento, sendo desnecessário uma nova habilitação.';

          messages.show({
            severity: 'warn',
            detail: msg,
            sticky: true,
            closable: false,
          });
        } else {
          form.get('nome').setValue(servidor.nome);
          form.get('matricula').setValue(servidor.matricula);
          form.get('classificacao').setValue(servidor.classificacao);
        }
      }
    }
  }

  async function handleSubmit() {
    await ServidorService.enable(form.value)
      .then(({ data: servidor }) => {
        form.reset();
        setFiltro({ ...filtro, cpf: servidor.cpf });

        toast.success('Servidor salvo com sucesso!');
      })
      .catch(error => ExceptionService.handle(error));
  }

  function handleRemove(servidor) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <ConfirmAlert
            onClose={onClose}
            onConfirm={async () => {
              await ServidorService.remove(servidor.codigo)
                .then(() => {
                  onClose();
                  toast.success('Servidor excluído com sucesso!');
                  loadServidores();
                })
                .catch(error => ExceptionService.handle(error));
            }}
            title="Confirmação"
            message="Deseja realmente excluir?"
          />
        );
      },
    });
  }

  return (
    <Content>
      <Navbar>
        <h1>Servidores</h1>
        <button onClick={() => setVisibleDialog(true)} type="button">
          Adicionar
        </button>
      </Navbar>

      <form onSubmit={e => e.preventDefault()}>
        <div className="p-grid p-fluid">
          <div className="p-col-12 p-md-4 p-sm-12">
            <InputMask
              mask="999.999.999-99"
              placeholder="CPF"
              value={filtro.cpf}
              onChange={e => setFiltro({ ...filtro, cpf: e.target.value })}
            />
          </div>

          <div className="p-col-12 p-md-8 p-sm-12">
            <InputText
              placeholder="Nome"
              value={filtro.nome}
              onChange={e => setFiltro({ ...filtro, nome: e.target.value })}
            />
          </div>

          {SecurityService.hasRole('ROLE_GERENCIAR_TODO_SERVIDOR') && (
            <div className="p-col-12 p-md-4 p-sm-12">
              <UnidadeGestoraMultiSelect
                filter
                placeholder="Unidades Gestoras"
                value={filtro.unidadesGestoras}
                onChange={e =>
                  setFiltro({ ...filtro, unidadesGestoras: e.target.value })
                }
              />
            </div>
          )}
          <div className="p-col-12 p-md-4 p-sm-12">
            <SelectButton
              value={filtro.status}
              options={[
                { value: 'ATUALIZADO', label: 'Atualizado' },
                { value: 'PENDENTE', label: 'Pendente' },
              ]}
              onChange={e => setFiltro({ ...filtro, status: e.target.value })}
            />
          </div>
        </div>
      </form>

      <DataTable
        lazy
        paginator
        responsive
        loading={loading}
        first={first}
        rows={rows}
        totalRecords={totalRecords}
        rowsPerPageOptions={[20, 50, 100]}
        value={servidores}
        onPage={onPage}
      >
        <Column field="cpf" header="CPF" style={{ textAlign: 'left' }} />
        <Column
          field="nome"
          header="Nome"
          style={{ textAlign: 'left', width: '30%' }}
        />
        <Column
          header="UG"
          style={{ textAlign: 'center' }}
          body={rowData => <>{rowData.unidadeGestora.codigoUg}</>}
        />
        <Column
          header="Habilitação"
          style={{ textAlign: 'center' }}
          body={rowData => <>{rowData.dataHabilitacao}</>}
        />
        <Column
          header="Atualização"
          style={{ textAlign: 'center' }}
          body={rowData => <>{rowData.dataHoraAtualizacao}</>}
        />
        <Column
          header="Status"
          style={{ textAlign: 'center' }}
          body={rowData => (
            <span
              className={
                rowData.atualizado
                  ? 'badge badge-success'
                  : 'badge badge-danger'
              }
            >
              {rowData.atualizado ? 'Atualizado' : 'Pendente'}
            </span>
          )}
        />
        <Column
          header="Ações"
          style={{ textAlign: 'center', width: 100 }}
          body={rowData => (
            <Button
              disabled={rowData.atualizado}
              type="button"
              icon="pi pi-trash"
              className="p-button-danger"
              onClick={() => handleRemove(rowData)}
            />
          )}
        />
      </DataTable>

      <Dialog
        modal
        header="Adicionar"
        style={{ width: 'auto' }}
        visible={visibleDialog}
        onHide={onHide}
      >
        <Messages
          ref={el => {
            messages = el;
          }}
        />

        <FieldGroup
          control={form}
          render={({ invalid }) => (
            <form>
              <div className="p-grid p-fluid">
                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="cpf">
                    {({ handler, touched, hasError }) => (
                      <>
                        <RInputMask
                          {...handler()}
                          className="p-inputtext p-component"
                          mask="999.999.999-99"
                          placeholder="CPF"
                          onBlur={e => {
                            handler().onBlur(e);
                            handleCpf(e.target.value);
                          }}
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="CPF obrigatório" />
                        )}
                        {touched && hasError('cpfInvalido') && (
                          <Message severity="error" text="CPF inválido" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="matricula">
                    {({ handler }) => (
                      <InputText
                        {...handler()}
                        maxLength="20"
                        placeholder="Matricula"
                      />
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="classificacao">
                    {({ handler }) => (
                      <Dropdown
                        {...handler()}
                        placeholder="Classificação"
                        options={classificacoes}
                      />
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-8 p-sm-12">
                  <FieldControl name="nome">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="50"
                          placeholder="Nome"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="Nome obrigatório" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                <div className="p-col-12 p-md-4 p-sm-12">
                  <FieldControl name="email">
                    {({ handler, touched, hasError }) => (
                      <>
                        <InputText
                          {...handler()}
                          maxLength="50"
                          placeholder="E-mail"
                        />
                        {touched && hasError('required') && (
                          <Message severity="error" text="E-mail obrigatório" />
                        )}
                        {touched && hasError('email') && (
                          <Message severity="error" text="E-mail inválido" />
                        )}
                      </>
                    )}
                  </FieldControl>
                </div>

                {SecurityService.hasRole('ROLE_GERENCIAR_TODO_SERVIDOR') && (
                  <div className="p-col-12 p-md-12 p-sm-12">
                    <FieldControl name="unidadeGestora">
                      {({ handler }) => (
                        <UnidadeGestoraDropdown
                          {...handler()}
                          filter
                          placeholder="Unidade Gestora"
                        />
                      )}
                    </FieldControl>
                  </div>
                )}
              </div>

              <div style={{ textAlign: 'right' }}>
                <Button
                  type="button"
                  disabled={invalid}
                  style={{ marginRight: 10 }}
                  label="Salvar"
                  icon="pi pi-check"
                  onClick={handleSubmit}
                />
                <Button
                  type="button"
                  className="p-button-danger"
                  label="Cancelar"
                  icon="pi pi-times"
                  onClick={onHide}
                />
              </div>
            </form>
          )}
        />
      </Dialog>
    </Content>
  );
}
