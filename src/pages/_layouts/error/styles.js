import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 0 30px;

  * {
    font-family: 'Roboto', monospace, sans-serif;
  }
`;

export const Content = styled.div`
  max-width: 1400px;
  margin: 50px auto;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
