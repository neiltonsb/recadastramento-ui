import React from 'react';
import PropTypes from 'prop-types';

import GlobalStyle from '~/styles/globalAdmin';
import { Wrapper, Content } from './styles';

export default function ErrorLayout({ children }) {
  return (
    <>
      <GlobalStyle />
      <Wrapper>
        <Content>{children}</Content>
      </Wrapper>
    </>
  );
}

ErrorLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
