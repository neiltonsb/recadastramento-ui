import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  * {
    font-family: 'Roboto', monospace, sans-serif;
  }
`;

export const Content = styled.div`
  width: 100%;
  max-width: 315px;
  text-align: center;

  header {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-bottom: 20px;
    padding-bottom: 20px;

    border-bottom: 1px solid #eee;

    img {
      width: 64px;
    }

    span {
      color: #333;
      font-size: 16px;
    }
  }
`;

export const Title = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  margin-bottom: 20px;

  img {
    margin-right: 10px;
  }

  span {
    color: #333;
  }
`;
