import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper, Content, Title } from './styles';

import GlobalStyle from '~/styles/globalAdmin';

import sefinLogo from '~/assets/logo-sefin.svg';
import registration from '~/assets/password.png';

export default function AuthLayout({ children }) {
  return (
    <>
      <GlobalStyle />
      <Wrapper>
        <Content>
          <header>
            <img src={sefinLogo} alt="" />
            <span>Superitendencia Estadual de Contabilidade</span>
          </header>

          <Title>
            <img src={registration} alt="" />
            <span>Sistema de Recadastramento</span>
          </Title>

          {children}
        </Content>
      </Wrapper>
    </>
  );
}

AuthLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
