import styled from 'styled-components';
import media from 'styled-media-query';

import Background from '~/assets/background.jpeg';

export const Wrapper = styled.div`
  height: 100%;
`;

export const Header = styled.div`
  padding: 0 30px;
  height: 400px;

  /* background-image: url(${Background});
  background-position-y: center;
  background-size: cover; */

  display: flex;
  flex-direction: column;
  align-content: center;

  &::before {
    content: '';
    background-image: url(${Background});
    background-position-y: center;
    background-size: cover;
    max-height: 400px;
    opacity: 0.5;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: absolute;
    z-index: -1;   
  }

  div {
    display: flex;
    justify-content: space-between;
    align-items: center;

    margin-top: 20px;

    img {
      width: 60px;
      height: 60px;
    }

    a {
      background: rgba(0, 0, 0, 0.5);
      color: #fff;

      padding: 12px;
      border-radius: 4px;
      font-weight: bold;

      &:hover {
        background: rgba(0, 0, 0, 0.8);
        color: #fff;
      }
    }
  }

  section {
    text-align: center;
    margin-top: 80px;

    p {
      font-size: 42px;
      color: #333;
      margin-bottom: 20px;

      ${media.lessThan('medium')`
        /* screen width is less than 768px (medium) */
        font-size: 28px;
      `}
    }

    a {
      background: rgba(0, 0, 0, 0.5);
      color: #fff;
      font-weight: bold;

      width: 250px;
      padding: 10px 0;
      border-radius: 4px;

      display: inline-block;

      font-size: 24px;

      &:hover {
        background: rgba(0, 0, 0, 0.8);
        color: #fff;
      }

      ${media.lessThan('medium')`
        /* screen width is less than 768px (medium) */
        font-size: 18px;
      `}
    }
  }
`;

export const Rotulo = styled.div`
  height: 320px;
  background-color: #006699;
  color: #fff;

  display: flex;
  flex-direction: column;

  ${media.greaterThan('830px')`  
    height: 200px;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    align-items: center;
  `}

  ${media.lessThan('320px')`  
    height: 350px;
    display: flex;
    flex-direction: column;
  `}
`;

export const Left = styled.div`
  padding: 20px;

  p:first-child {
    margin-bottom: 10px;
  }

  p {
    font-size: 14px;
    vertical-align: middle;
    text-align: justify;
  }

  ${media.greaterThan('650px')`
    border-right: 1px solid #eee;
    p {
      font-size: 16px;
    }
  `}
`;

export const Right = styled.div`
  padding: 20px;

  display: flex;
  justify-content: space-around;

  div {
    align-items: center;
    display: flex;
    flex-direction: column;

    color: #eee;

    b {
      font-size: 10px;
    }

    span {
      margin-top: 5px;
      padding: 10px;
      border: 1px solid rgba(255, 255, 255, 0.2);
      font-size: 12px;
    }

    ${media.greaterThan('650px')`  
      b {
        font-size: 14px;
      }

      span {
        font-size: 18px;
      }
    `}
  }
`;

export const Footer = styled.div`
  border-top: 1px solid #eee;

  div {
    margin: 20px 0 50px 0;
    display: flex;
    flex-direction: column;
    align-items: center;

    p {
      text-align: center;
      font-weight: bold;
      color: #333;
      margin-bottom: 10px;
    }

    span {
      font-size: 12px;
      color: #333;
      margin-bottom: 10px;
    }

    img {
      width: 60px;
    }

    ${media.greaterThan('650px')`  
      p {
        font-size: 24px;
      }

      span {
        font-size: 14px;
      }

      img {
        width: 90px;
      }
    `}
  }
`;
