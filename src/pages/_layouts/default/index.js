import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';
import { format, parseISO, differenceInDays } from 'date-fns';

import { Wrapper, Header, Rotulo, Left, Right, Footer } from './styles';

import GlobalStyle from '~/styles/globalDefault';

import sefinLogo from '~/assets/logo-sefin.svg';
import govLogo from '~/assets/brasao-rondonia-grande.png';

import * as CampaignService from '~/services/campaign';
import * as ExceptionService from '~/services/exception';

export default function DefaultLayout({ children }) {
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [days, setDays] = useState('');
  const [expired, setExpired] = useState(true);

  useEffect(() => {
    async function loadCampaign() {
      const { data: campaign } = await CampaignService.get().catch(error =>
        ExceptionService.handle(error)
      );

      const start = format(parseISO(campaign.dataInicio), 'dd/MM/yyyy');
      const end = format(parseISO(campaign.dataFim), 'dd/MM/yyyy');

      setStartDate(start);
      setEndDate(end);
      setExpired(campaign.expired);
      setDays(differenceInDays(parseISO(campaign.dataFim), new Date()));
    }

    loadCampaign();
  });

  return (
    <>
      <Wrapper>
        <Header>
          <div>
            <img src={sefinLogo} alt="" />

            <Link to="/login">AREA RESTRITA</Link>
          </div>

          <section>
            <p>
              Campanha de <strong>Recadastramento</strong> do SIAFEM
            </p>

            <a href="#recadastramento">FAÇA AGORA</a>
          </section>
        </Header>
        <Rotulo>
          <Left>
            <p>
              Começou a campanha de recadastramento 2019, peça ao contador
              responsável de sua Unidade Gestora para habilitá-lo, assim, você
              estará apto para realizar este processo.
            </p>
            <p>
              Lembrando que a não realização do recadastramento até a data
              estipulada implicará diretamente no seu acesso ao SIAFEM.
            </p>
          </Left>
          <Right>
            <div>
              <b>INÍCIO DA CAMPANHA</b>
              <span>{startDate}</span>
            </div>
            <div>
              <b>TERMINO DA CAMPANHA</b>
              <span className={expired && 'blinker'}>{endDate}</span>
            </div>
            <div>
              <b>RESTAM</b>
              <span className={expired && 'blinker'}>
                {days <= 0 ? `0 dias` : `${days} dia(s)`}{' '}
              </span>
            </div>
          </Right>
        </Rotulo>
        {children}
        <Footer>
          <div>
            <p>Superitendência Estadual de Contabilidade</p>
            <span>© 2019 - Governo do Estado de Rondônia</span>
            <img src={govLogo} alt="Gov" />
          </div>
        </Footer>
      </Wrapper>
      <GlobalStyle />
    </>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
