import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Sidebar } from 'primereact/sidebar';
import { signOut } from '~/store/modules/auth/actions';

import * as SecurityService from '~/services/security';

import {
  Wrapper,
  Container,
  Content,
  SystemMenu,
  MenuItem,
  UserMenu,
} from './styles';
import Header from '~/components/Header';
import GlobalStyle from '~/styles/globalAdmin';

export default function AdminLayout({ children }) {
  const [sideBar, setSideBar] = useState(false);

  const { pathname } = useLocation();
  const dispatch = useDispatch();

  return (
    <>
      <Wrapper>
        <Header sideBar={sideBar} setSideBar={setSideBar} />
        <Sidebar visible={sideBar} onHide={() => setSideBar(false)}>
          <SystemMenu>
            <MenuItem
              visible={SecurityService.hasAnyRole([
                'ROLE_GERENCIAR_TODO_SERVIDOR',
                'ROLE_GERENCIAR_SERVIDOR_VINCULADO_UG',
              ])}
              active={pathname === '/admin/servidores'}
            >
              <Link to="/admin/servidores" onClick={() => setSideBar(false)}>
                Servidores
              </Link>
            </MenuItem>
            <MenuItem
              visible={SecurityService.hasRole('ROLE_GERENCIAR_USUARIO')}
              active={pathname === '/admin/usuarios'}
            >
              <Link to="/admin/usuarios" onClick={() => setSideBar(false)}>
                Usuários
              </Link>
            </MenuItem>
            <MenuItem
              visible={SecurityService.hasRole('ROLE_GERENCIAR_FORMACAO')}
              active={pathname === '/admin/formacoes'}
            >
              <Link to="/admin/formacoes" onClick={() => setSideBar(false)}>
                Formações
              </Link>
            </MenuItem>
            <MenuItem
              visible={SecurityService.hasRole('ROLE_GERENCIAR_CARGO')}
              active={pathname === '/admin/cargos'}
            >
              <Link to="/admin/cargos" onClick={() => setSideBar(false)}>
                Cargos
              </Link>
            </MenuItem>
            <MenuItem
              visible={SecurityService.hasRole(
                'ROLE_GERENCIAR_UNIDADE_GESTORA'
              )}
              active={pathname === '/admin/unidades_gestoras'}
            >
              <Link
                to="/admin/unidades_gestoras"
                onClick={() => setSideBar(false)}
              >
                Unidades Gestoras
              </Link>
            </MenuItem>
            <MenuItem
              visible={SecurityService.hasRole('ROLE_GERENCIAR_CAMPANHA')}
              active={pathname === '/admin/campanhas'}
            >
              <Link to="/admin/campanhas" onClick={() => setSideBar(false)}>
                Campanhas
              </Link>
            </MenuItem>
          </SystemMenu>
          <UserMenu>
            <li>
              <Link to="/admin/perfil" onClick={() => setSideBar(false)}>
                Perfil
              </Link>
            </li>
            <li>
              <a
                href={process.env.REACT_APP_ROUTER_BASE || '/login'}
                onClick={() => dispatch(signOut())}
              >
                Sair
              </a>
            </li>
          </UserMenu>
        </Sidebar>
        <Container>
          <Content>{children}</Content>
        </Container>
      </Wrapper>
      <GlobalStyle />
    </>
  );
}

AdminLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
