import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
`;

export const Container = styled.div`
  padding: 0 30px;
`;

export const Content = styled.div`
  max-width: 1400px;
  margin: 0 auto;
`;

export const SystemMenu = styled.ul`
  padding: 30px 0 15px 0;
  border-bottom: 1px solid #eee;

  * {
    font-family: 'Roboto', monospace, sans-serif;
  }
`;

export const MenuItem = styled.li`
  display: ${props => (props.visible ? 'flex' : 'none')};

  a {
    width: 100%;
    color: #006699;
    padding: 15px;

    &:hover {
      background: #eee;
    }
  }

  ${props =>
    props.active &&
    css`
      a {
        background: #006699;
        color: #fff;
        pointer-events: none;
      }
    `}
`;

export const UserMenu = styled.ul`
  padding: 15px 0;

  li {
    display: flex;

    a {
      width: 100%;
      padding: 10px;
      color: #006699;

      &:hover {
        color: #888;
      }
    }
  }
`;
