import React from 'react';
import history from '~/services/history';

import { ErrorCode, ErrorDescription, Button } from './styles';

export default function Forbidden() {
  return (
    <>
      <ErrorCode>403</ErrorCode>
      <ErrorDescription>
        Opaaa desculpe... <br />
        Você não tem permissão para acessar a página solicitada. Se precisar de
        ajuda, entre em contato conosco.
      </ErrorDescription>
      <Button type="button" onClick={() => history.goBack().goBack()}>
        Voltar
      </Button>
    </>
  );
}
