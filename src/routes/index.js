import React from 'react';

import { BrowserRouter, Switch } from 'react-router-dom';

import Route from './Route';
import Home from '~/pages/Home';
import Dashboard from '~/pages/Dashboard';
import User from '~/pages/User';
import Perfil from '~/pages/Perfil';
import SigIn from '~/pages/SignIn';
import Graduation from '~/pages/Graduation';
import Work from '~/pages/Work';
import Ug from '~/pages/Ug';
import Campaign from '~/pages/Campaign';
import NoMatch from '~/pages/NoMatch';
import Forbidden from '~/pages/Forbidden';
import CampaignExpired from '~/pages/CampaignExpired';

export default function routes() {
  return (
    <BrowserRouter basename={process.env.REACT_APP_ROUTER_BASE || ''}>
      <Switch>
        <Route path="/" exact component={Home} title="Página Inicial" />
        <Route path="/login" component={SigIn} title="Login" />
        <Route
          path="/admin/servidores"
          component={Dashboard}
          isPrivate
          title="Servidores"
        />
        <Route
          path="/admin/usuarios"
          component={User}
          isPrivate
          title="Usuários"
        />
        <Route
          path="/admin/perfil"
          component={Perfil}
          isPrivate
          title="Perfil"
        />
        <Route
          path="/admin/formacoes"
          component={Graduation}
          isPrivate
          title="Formações"
        />
        <Route path="/admin/cargos" component={Work} isPrivate title="Cargos" />
        <Route
          path="/admin/unidades_gestoras"
          component={Ug}
          isPrivate
          title="Unidades Gestoras"
        />
        <Route
          path="/admin/campanhas"
          component={Campaign}
          isPrivate
          title="Campanhas"
        />
        <Route
          path="/campanha_encerrada"
          component={CampaignExpired}
          title="Campanha Encerrada"
        />
        <Route path="/403" component={Forbidden} title="Não Autorizado" />
        <Route path="*" component={NoMatch} title="Página não encontrada" />
      </Switch>
    </BrowserRouter>
  );
}
