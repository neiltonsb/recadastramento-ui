import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

import AdminLayout from '~/pages/_layouts/admin';
import DefaultLayout from '~/pages/_layouts/default';
import AuthLayout from '~/pages/_layouts/auth';
import ErrorLayout from '~/pages/_layouts/error';

import { store } from '~/store';

export default function RouteWrapper({
  component: Component,
  isPrivate,
  path,
  title,
  ...rest
}) {
  window.document.title = title;

  let Layout = DefaultLayout;
  const { signed } = store.getState().auth;

  /* Verifiy User Signed */
  if (!signed && isPrivate) {
    return <Redirect to="/login" />;
  }

  /* Define Layout */
  if (path === '/') {
    Layout = DefaultLayout;
  } else if (path === '/login') {
    Layout = AuthLayout;
  } else if (
    path === '*' ||
    path === '/403' ||
    path === '/campanha_encerrada'
  ) {
    Layout = ErrorLayout;
  } else {
    Layout = AdminLayout;
  }

  return (
    <Route
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
}

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  path: PropTypes.string.isRequired,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
    .isRequired,
  title: PropTypes.string.isRequired,
};

RouteWrapper.defaultProps = {
  isPrivate: false,
};
